﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace Logger
{
    public class Log : Ilog
    {
        public DateTime Time { get; set; } = DateTime.Now;
        public string Type { get; set; }
        public string MessageType { get; set; }
        public string Message { get; set; }

        public void WriteLog(Exception ex)
        {
            Type = ex.GetType().ToString();
            MessageType = ex.Message.GetType().ToString();
            Message = ex.Message;

            using (FileStream stream = File.Open($"{DateTime.Now.ToShortDateString()}.log",FileMode.Append))
            {
                byte[] data = new byte[stream.Length];
                string dataInfo = $"{Time} {MessageType} {Type} - {Message}\n";
                data = Encoding.Default.GetBytes(dataInfo);
                stream.Write(data, 0, data.Length);
            }

        }

        public void WriteLogToJson(Exception ex)
        {
            Type = ex.GetType().ToString();
            MessageType = ex.Message.GetType().ToString();
            Message = ex.Message;

            using (FileStream stream = File.Open($"{DateTime.Now.ToShortDateString()}.json", FileMode.Append))
            {
                byte[] data = new byte[stream.Length];
                string jsonDataInfo = JsonConvert.SerializeObject(ex);
                data = Encoding.Default.GetBytes(jsonDataInfo);
                stream.Write(data, 0, data.Length);
            }
        }
    }
}
