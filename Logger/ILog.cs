﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logger
{
    public interface Ilog
    {
        public void WriteLog(Exception ex);
        public void WriteLogToJson(Exception ex);
    }
}
